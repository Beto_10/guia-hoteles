$(function(){
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
  $('.carousel').carousel({
    interval:2000
  });



  $('body #InfoHoteles').on('click', 'Button', function (){ //para saber que boton se le dio click

    var BotoId = "#" + $(this).attr('id');

    //show es cuando el modal se muestra---------------
    $('#contacto').on('show.bs.modal', function (e){
      
      console.log('el modal se muestra');
      //QUITAR UNA CLASE DEL BOTON 
      $(BotoId).removeClass('btn-outline-success');
      //AGREGAR UNA CLASE
      $(BotoId).addClass('btn-primary');
      //DESHABILITAR UN BOTON
      $(BotoId).prop('disabled',true);      

    }); 

    //shown es cuando el modal se mostró---------------
    $('#contacto').on('shown.bs.modal', function (e){
      console.log('el modal se mostró');
    });
    
    //-----------------------------------------------------

    //hide es cuando el modal ya se oculta---------------
     $('#contacto').on('hide.bs.modal', function (e){
      console.log('el modal se oculta');
     });
    //hidden es cuando el modal ya se ocultó---------------
    $('#contacto').on('hidden.bs.modal', function (e){
      
      console.log('el modal se ocultó');
    //AGREGAR UNA CLASE
      $(BotoId).addClass('btn-outline-success');
    //QUITAR UNA CLASE DEL BOTON 
      $(BotoId).removeClass('btn-primary');
    //HABILITAR UN BOTON
      $(BotoId).prop('disabled',false);

      $("#contacto").modal("dispose");

    });
    //-----------------------------------------------------

    //hidden es cuando el modal ya se ocultó---------------
    $('#reservacion').on('show.bs.modal', function (e){

      //DESHABILITAR UN BOTON
      $(BotoId).prop('disabled',true);   

    }); 
    
    //-----------------------------------------------------

    //hidden es cuando el modal ya se ocultó---------------
    $('#reservacion').on('hidden.bs.modal', function (e){
      
    $("#contacto").modal("dispose");
    //HABILITAR UN BOTON
      $(BotoId).prop('disabled',false);

      $("#reservacion").modal("dispose");

    });
    //-----------------------------------------------------

  });

});